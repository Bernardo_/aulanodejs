const express = require('express');
const path = require('path');
const app = express();
const PORT = 3000;

app.use(express.static(path.join(__dirname, 'public')));

// Rota para a página inicial
app.get('/', (req, res) => {
  res.sendFile(path.join(__dirname, 'public', 'index.html'));
});

// Rota para a página "Objetivo"
app.get('/objetivo', (req, res) => {
  res.sendFile(path.join(__dirname, 'public', 'objetivo.html'));
});

// Rota para a página "Desenvolvimento"
app.get('/desenvolvimento', (req, res) => {
  res.sendFile(path.join(__dirname, 'public', 'desenvolvimento.html'));
});

// Rota para a página "Conclusão"
app.get('/conclusao', (req, res) => {
  res.sendFile(path.join(__dirname, 'public', 'conclusao.html'));
});

app.listen(PORT, () => {
  console.log(`Servidor rodando em http://localhost:${PORT}`);
});
